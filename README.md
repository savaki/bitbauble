bitbauble
---------

bitbauble is a set of tools to simplify using Bitbucket Pipelines

### Installation

Download the binary from bitbucket:

* Mac - [bitbauble0.1.0.darwin-amd64](https://bitbucket.org/savaki/bitbauble/downloads/bitbauble0.1.0.darwin-amd64)
* Linux - [bitbauble0.1.0.linux-amd64](https://bitbucket.org/savaki/bitbauble/downloads/bitbauble0.1.0.linux-amd64)
* Windows - [bitbauble0.1.0.windows-amd64](https://bitbucket.org/savaki/bitbauble/downloads/bitbauble0.1.0.windows-amd64)

If you use go, you can grab the latest version from:

```
go get bitbucket.org/savaki/bitbauble
```

### Setup

bitbauble uses AWS dynamodb to store the build numbers.  Consequently, before you can 
use bitbauble, you'll need to set up the dynamodb table.  The simplest way to do this 
is to use the built in ```bitbauble infra command```

```
bitbauble infra
```

Infra will assume the presence of the following AWS properties:

* ```AWS_ACCESS_KEY_ID```
* ```AWS_SECRET_ACCESS_KEY```

Alternately, being on an ec2 instance with a role with the appropriate permissions works as well.

### Generate Build Numbers

Generate a monotonically increasing build number.


```
  Generate the next build for the key, "user/project"
  $ bitbauble generate --key user/project

  Generate the next build for the key, "user/project" and save the env variable, BUILD_NUMBER
  $ BUILD_NUMBER=$(bitbauble generate --key user/project)

  Generate the next build for the key, "user/project".  Start with 10
  $ bitbauble generate --key user/project --start 10
```

### Usage 

```
NAME:
   bitbauble - provides a set of utilities for Bitbucket Pipelines

USAGE:
   bitbauble [global options] command [command options] [arguments...]

VERSION:
   0.1.0

COMMANDS:
     generate  generates a monotically increasing build number
     infra     create the aws infrastructure
     help, h   Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```