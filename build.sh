#!/bin/bash

VERSION=0.1.0

for arch in darwin linux windows
do
    echo "building ${arch}"
    GOOS=${arch} go build -o bitbauble${VERSION}.${arch}-amd64
done

