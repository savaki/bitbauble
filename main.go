package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/urfave/cli"
)

const (
	version               = "0.1.0"
	errNotFound           = "ConditionalCheckFailedException"
	errTableAlreadyExists = "ResourceInUseException"
)

// Options provides a container to locally stored options
type Options struct {
	Key   string
	Start int
	AWS   struct {
		Region        string
		TableName     string
		WriteCapacity int64
	}
}

var opts Options

func main() {
	app := cli.NewApp()
	app.Name = "bitbauble"
	app.Usage = "provides a set of utilities for Bitbucket Pipelines"
	app.Version = version
	app.Commands = []cli.Command{
		{
			Name:  "generate",
			Usage: "generates a monotically increasing build number",
			Description: `
Some examples.

	Generate the next build for the key, "user/project"
	$ bitbauble generate --key user/project

	Generate the next build for the key, "user/project" and save the env variable, BUILD_NUMBER
	$ BUILD_NUMBER=$(bitbauble generate --key user/project)

	Generate the next build for the key, "user/project".  Start with 10
	$ bitbauble generate --key user/project --start 10`,
			Action: Generate,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "key",
					Usage:       "a unique string that identifies the build number sequence",
					Destination: &opts.Key,
				},
				cli.IntFlag{
					Name:        "start",
					Value:       1,
					Usage:       "the starting value of the sequence",
					Destination: &opts.Start,
				},
				cli.StringFlag{
					Name:        "table",
					Value:       "bitbauble",
					Usage:       "name of dynamodb table",
					Destination: &opts.AWS.TableName,
				},
				cli.StringFlag{
					Name:        "region",
					Value:       "us-east-1",
					Usage:       "AWS region that contains dynamodb table",
					Destination: &opts.AWS.Region,
				},
			},
		},
		{
			Name:  "infra",
			Usage: "create the aws infrastructure",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "table",
					Value:       "bitbauble",
					Usage:       "name of dynamodb table",
					Destination: &opts.AWS.TableName,
				},
				cli.StringFlag{
					Name:        "region",
					Value:       "us-east-1",
					Usage:       "AWS region that contains dynamodb table",
					Destination: &opts.AWS.Region,
				},
				cli.Int64Flag{
					Name:        "capacity",
					Value:       3,
					Usage:       "dynamodb table write capacity; bitbauble uses writes almost exclusively",
					Destination: &opts.AWS.WriteCapacity,
				},
			},
			Action: Infra,
		},
	}
	app.Run(os.Args)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func putItem(api *dynamodb.DynamoDB) (string, error) {
	_, err := api.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String(opts.AWS.TableName),
		Item: map[string]*dynamodb.AttributeValue{
			"key":   {S: aws.String("seq:" + opts.Key)},
			"value": {N: aws.String(strconv.Itoa(opts.Start))},
		},
		ConditionExpression: aws.String("attribute_not_exists(#key)"),
		ExpressionAttributeNames: map[string]*string{
			"#key": aws.String("key"),
		},
	})
	if err != nil {
		return "", err
	}

	return strconv.Itoa(opts.Start), nil
}

func updateItem(api *dynamodb.DynamoDB) (string, error) {
	out, err := api.UpdateItem(&dynamodb.UpdateItemInput{
		TableName: aws.String(opts.AWS.TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"key": {S: aws.String("seq:" + opts.Key)},
		},
		UpdateExpression:    aws.String("ADD #value :one"),
		ConditionExpression: aws.String("attribute_exists(#key)"),
		ExpressionAttributeNames: map[string]*string{
			"#key":   aws.String("key"),
			"#value": aws.String("value"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":one": {N: aws.String("1")},
		},
		ReturnValues: aws.String("ALL_NEW"),
	})
	if err != nil {
		return "", err
	}

	return *out.Attributes["value"].N, nil
}

// Generate prints the next id in the sequence
func Generate(_ *cli.Context) error {
	api := API()

	n, err := updateItem(api)
	if err != nil {
		if v, ok := err.(awserr.Error); ok && v.Code() == errNotFound {
			n, err = putItem(api)
			check(err)

		} else {
			check(err)
			return err
		}
	}

	fmt.Print(n)

	return nil
}

// Infra generates a new thing
func Infra(_ *cli.Context) error {
	api := API()
	fmt.Printf("Creating DynamoDB table, %v [%v]\n", opts.AWS.TableName, opts.AWS.Region)
	_, err := api.CreateTable(&dynamodb.CreateTableInput{
		TableName: aws.String(opts.AWS.TableName),
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("key"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("key"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(3),
			WriteCapacityUnits: aws.Int64(opts.AWS.WriteCapacity),
		},
	})
	if err != nil {
		if v, ok := err.(awserr.Error); ok && v.Code() == errTableAlreadyExists {
			fmt.Println("... table already exists, nothing to do")
			fmt.Println("Ok")
			return nil
		}
		check(err)
	}
	fmt.Println("Ok")

	return nil
}

// Api returns a reference to the AWS dynamodb api
func API() *dynamodb.DynamoDB {
	cfg := &aws.Config{Region: aws.String(opts.AWS.Region)}
	s, err := session.NewSession(cfg)
	check(err)

	return dynamodb.New(s)
}
